This is scheme48.info, produced by makeinfo version 6.7 from
scheme48.texi.

This manual is for Scheme48 version 1.3.

   Copyright (C) 2004, 2005, 2006 Taylor Campbell.  All rights reserved.

   This manual includes material derived from works bearing the
following notice:

   Copyright (C) 1993-2005 Richard Kelsey, Jonathan Rees, and Mike
Sperber.  All rights reserved.

     Redistribution and use in source and binary forms, with or without
     modification, are permitted provided that the following conditions
     are met:

        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.

        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials
          provided with the distribution.

        * The name of the authors may not be used to endorse or promote
          products derived from this software without specific prior
          written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

INFO-DIR-SECTION The Algorithmic Language Scheme
START-INFO-DIR-ENTRY
* Scheme48: (scheme48).	Nearly complete reference manual for version 1.3
END-INFO-DIR-ENTRY


File: scheme48.info,  Node: Structure index,  Prev: Binding index,  Up: Top

Structure index
***************

 [index ]
* Menu:

* architecture:                          Miscellaneous I/O internals.
                                                              (line   6)
* arrays:                                Library data structures.
                                                              (line  11)
* ascii:                                 ASCII character encoding.
                                                              (line   6)
* big-util:                              Library utilities.   (line 109)
* bitwise:                               Bitwise manipulation.
                                                              (line  13)
* byte-vectors:                          Bitwise manipulation.
                                                              (line  36)
* cells:                                 Cells.               (line   6)
* channel-i/o:                           Channels.            (line 108)
* channel-ports:                         Channel ports.       (line   6)
* channels:                              Channels.            (line  15)
* code-quote:                            Miscellaneous features.
                                                              (line  90)
* conditions:                            Condition system.    (line   6)
* condvars:                              Higher-level synchronization.
                                                              (line  12)
* debug-messages:                        Miscellaneous features.
                                                              (line  57)
* define-record-types:                   Records.             (line  10)
* define-sync-record-types:              Optimistic concurrency.
                                                              (line 143)
* defrecord:                             Records.             (line  10)
* destructuring:                         Library utilities.   (line  12)
* dump/restore:                          Library utilities.   (line 257)
* dynamic-externals:                     C interface.         (line  49)
* dynamic-externals <1>:                 Dynamic loading of C modules.
                                                              (line  69)
* enum-case:                             Integer enumerations.
                                                              (line  43)
* enum-sets:                             Enumerated/finite types and sets.
                                                              (line   9)
* enumerated:                            Integer enumerations.
                                                              (line   6)
* exceptions:                            Condition system.    (line   6)
* extended-ports:                        I/O extensions.      (line   6)
* external-calls:                        C interface.         (line  49)
* external-calls <1>:                    Calling C functions from Scheme.
                                                              (line   6)
* features:                              Miscellaneous features.
                                                              (line   6)
* filenames:                             Filenames.           (line   6)
* finite-types:                          Enumerated/finite types and sets.
                                                              (line   9)
* fluids:                                Fluid/dynamic bindings.
                                                              (line   6)
* formats:                               Common-Lisp-style formatting.
                                                              (line   6)
* handle:                                Condition system.    (line 125)
* i/o:                                   Ports.               (line  18)
* i/o-internal:                          Ports.               (line 108)
* i/o-internal <1>:                      Port handlers.       (line   6)
* i/o-internal <2>:                      Buffered ports & handlers.
                                                              (line   6)
* i/o-internal <3>:                      Miscellaneous I/O internals.
                                                              (line   6)
* list-interfaces:                       Using the module system.
                                                              (line 126)
* load-dynamic-externals:                C interface.         (line  49)
* load-dynamic-externals <1>:            Dynamic loading of C modules.
                                                              (line   6)
* locks:                                 Pessimistic concurrency.
                                                              (line  17)
* loopholes:                             Type annotations.    (line   6)
* mask-types:                            Boxed bitwise-integer masks.
                                                              (line   6)
* masks:                                 Boxed bitwise-integer masks.
                                                              (line   6)
* meta-methods:                          Generic dispatch system.
                                                              (line   6)
* methods:                               Generic dispatch system.
                                                              (line   6)
* methods <1>:                           Writer.              (line  58)
* mvlet:                                 Library utilities.   (line 228)
* nondeterminism:                        Library utilities.   (line  83)
* placeholders:                          Higher-level synchronization.
                                                              (line  64)
* ports:                                 Port data type.      (line   6)
* posix:                                 POSIX interface.     (line  37)
* posix-files:                           POSIX interface.     (line  22)
* posix-files <1>:                       POSIX file system access.
                                                              (line   6)
* posix-i/o:                             POSIX interface.     (line  25)
* posix-i/o <1>:                         POSIX I/O utilities. (line   6)
* posix-platform-names:                  POSIX host OS and machine identification.
                                                              (line   6)
* posix-process-data:                    POSIX interface.     (line  19)
* posix-process-data <1>:                POSIX process environment.
                                                              (line   6)
* posix-processes:                       POSIX interface.     (line  16)
* posix-processes <1>:                   POSIX processes.     (line   6)
* posix-regexps:                         POSIX interface.     (line  34)
* posix-regexps <1>:                     POSIX regular expressions.
                                                              (line  17)
* posix-signals:                         POSIX signals.       (line   6)
* posix-time:                            POSIX interface.     (line  28)
* posix-users:                           POSIX interface.     (line  31)
* posix-users <1>:                       POSIX users and groups.
                                                              (line   6)
* pp:                                    Library utilities.   (line  29)
* prescheme:                             Module system architecture.
                                                              (line  40)
* prescheme <1>:                         Standard Pre-Scheme environment.
                                                              (line   6)
* prescheme <2>:                         Running Pre-Scheme as Scheme.
                                                              (line   6)
* prescheme-compiler:                    Invoking the Pre-Scheme compiler.
                                                              (line  40)
* proposals:                             Optimistic concurrency.
                                                              (line  34)
* ps-flonums:                            Pre-Scheme floating point operation.
                                                              (line   6)
* ps-flonums <1>:                        Running Pre-Scheme as Scheme.
                                                              (line   6)
* ps-memory:                             Low-level Pre-Scheme memory manipulation.
                                                              (line   6)
* ps-memory <1>:                         Running Pre-Scheme as Scheme.
                                                              (line   6)
* ps-receive:                            Multiple return values in Pre-Scheme.
                                                              (line   6)
* ps-receive <1>:                        Running Pre-Scheme as Scheme.
                                                              (line   6)
* ps-record-types:                       Pre-Scheme record types.
                                                              (line   6)
* ps-record-types <1>:                   Running Pre-Scheme as Scheme.
                                                              (line   6)
* queues:                                Queues.              (line   6)
* reading:                               Reader.              (line   6)
* receiving:                             Library utilities.   (line 213)
* record-types:                          Records.             (line 134)
* records:                               Records.             (line 216)
* records-internal:                      Records.             (line 186)
* reduce:                                Macros for writing loops.
                                                              (line   9)
* regexp:                                POSIX regular expressions.
                                                              (line  76)
* rendezvous:                            Rendezvous base combinators.
                                                              (line   6)
* rendezvous-async-channels:             Rendezvous communication channels.
                                                              (line  46)
* rendezvous-channels:                   Rendezvous communication channels.
                                                              (line   9)
* rendezvous-jars:                       Rendezvous-synchronized cells.
                                                              (line  39)
* rendezvous-placeholders:               Rendezvous-synchronized cells.
                                                              (line   9)
* rendezvous-time:                       Rendezvous base combinators.
                                                              (line  59)
* scheme:                                Module system architecture.
                                                              (line  40)
* search-trees:                          Library data structures.
                                                              (line  54)
* shared-bindings:                       C interface.         (line  49)
* shared-bindings <1>:                   Shared bindings between Scheme and C.
                                                              (line   6)
* silly:                                 Miscellaneous features.
                                                              (line  46)
* simple-conditions:                     Condition system.    (line 166)
* simple-signals:                        Condition system.    (line  65)
* sockets:                               TCP & UDP sockets.   (line   6)
* sockets <1>:                           TCP & UDP sockets.   (line  27)
* sparse-vectors:                        Library data structures.
                                                              (line 103)
* spatial:                               Resource statistics and control.
                                                              (line  60)
* srfi-7:                                SRFI 7.              (line   6)
* srfi-9:                                Records.             (line  10)
* strong:                                Library utilities.   (line  64)
* structure-refs:                        Module configuration language.
                                                              (line  23)
* tables:                                Hash tables.         (line   6)
* threads:                               Basic thread operations.
                                                              (line   6)
* threads-internal:                      Custom thread synchronization.
                                                              (line   6)
* time:                                  Library utilities.   (line 283)
* traverse:                              Resource statistics and control.
                                                              (line  83)
* udp-sockets:                           TCP & UDP sockets.   (line   6)
* udp-sockets <1>:                       TCP & UDP sockets.   (line  47)
* usual-resumer:                         Suspending and resuming heap images.
                                                              (line  41)
* util:                                  Various utilities.   (line   6)
* weak:                                  Weak references.     (line   6)
* write-images:                          Suspending and resuming heap images.
                                                              (line  23)
* writing:                               Writer.              (line   6)

