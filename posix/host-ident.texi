@node POSIX host OS and machine identification
@section Host OS and machine identification

@stindex posix-platform-names
@deffn procedure host-name @returns{} string
@deffnx procedure os-node-name @returns{} string
@deffnx procedure os-release-name @returns{} string
@deffnx procedure os-version-name @returns{} string
@deffnx procedure machine-name @returns{} string
These procedures return strings that are intended to identify various
aspects of the current operating system and physical machine.  POSIX
does not specify the format of the strings.  These procedures are
provided by both the structure @code{posix-platform-names} and the
structure @code{posix}.
@end deffn
