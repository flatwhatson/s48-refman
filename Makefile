# Build the Scheme48 Reference Manual

MAKEINFO=makeinfo
MAKEINFO_INFO=$(MAKEINFO)
MAKEINFO_HTML=$(MAKEINFO) --html --no-split
MAKEINFO_PDF=$(MAKEINFO) --pdf

TARGETS= scheme48

scheme48_SOURCES:= scheme48.texi macros.texi intro-ack.texi \
                   $(shell find -mindepth 2 -type f -name '*.texi')

all: info html pdf

info: $(TARGETS:=.info)
html: $(TARGETS:=.html)
pdf: $(TARGETS:=.pdf)

%.info: %.texi $(%_SOURCES)
	@rm -f $@ $@-* $@.log
	$(MAKEINFO_INFO) $< -o $@ &>$@.log

%.html: %.texi $(%_SOURCES)
	@rm -f $@ $@.log
	$(MAKEINFO_HTML) $< -o $@ &>$@.log

%.pdf: %.texi $(%_SOURCES)
        # Rendering a PDF generates a huge amount of intermediate
        # garbage, so we use a temp directory, smoke, and mirrors.
	@rm -f $@ $@.log
	@tmp=$$(mktemp --tmpdir -d $@.XXXXX);         \
	src=$$(realpath $<);                          \
	echo "$(MAKEINFO_PDF) $< -o $@ &>$@.log";     \
	( cd $$tmp                                    \
           && $(MAKEINFO_PDF) $$src -o $@ &>$@.log )  \
            && mv $$tmp/$@ .;                         \
	mv $$tmp/$@.log .;                            \
	rm -rf $$tmp
	@test -e $@

clean:
	rm -f $(TARGETS:=.info) $(TARGETS:=.info-*) $(TARGETS:=.info.log)
	rm -f $(TARGETS:=.html) $(TARGETS:=.html.log)
	rm -f $(TARGETS:=.pdf) $(TARGETS:=.pdf.log)

.PHONY: default all info html pdf clean
