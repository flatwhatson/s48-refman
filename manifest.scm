(use-modules (guix profiles)
             (gnu packages base)
             (gnu packages tex)
             (gnu packages texinfo))

(packages->manifest
 (list findutils
       gnu-make
       texinfo
       texlive-base
       texlive-tex-texinfo))
