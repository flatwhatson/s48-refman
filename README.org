* The Nearly Complete Scheme48 Reference Manual

This is a reference manual for Scheme48 and Pre-Scheme version 1.3
written by Taylor Campbell in 2005-2007.  It has been updated to build
with recent versions of Texinfo, and a Makefile has been added to
produce Info, HTML, and PDF versions.

The latest HTML manual is available here: [[https://tailcall.au/s48-refman/scheme48.html][scheme48.html]]

The latest PDF manual is available here: [[https://tailcall.au/s48-refman/scheme48.pdf][scheme48.pdf]]

** How to build

The Guix manifest sets up all prerequisites, and compilation is handled
by the Makefile.

To build the manual yourself:

#+BEGIN_SRC sh
git clone https://notabug.org/flatwhatson/s48-refman.git
cd s48-refman
guix shell -m manifest.scm
make # info html pdf
#+END_SRC

** License

This manual is licensed under BSD-3-Clause.  See COPYING for details.
